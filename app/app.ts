import express from 'express';

import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import {WelcomeController} from './controllers';
import {IndexController} from './controllers';

const app: express.Application = express();
const port: number =  3000;

// use public as static folder for plain html, css and client-side js.
app.use(express.static(path.join(__dirname, '../app/public')));
// use pug as view engine
app.set('view engine', 'pug');
app.set('views', path.join(__dirname , '../app/views'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ type: 'application/vnd.api+json' }))
app.use(cookieParser());

// setting up the routes
app.use('/welcome', WelcomeController);
app.use('/', IndexController);

// start the server
app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/`);
});
